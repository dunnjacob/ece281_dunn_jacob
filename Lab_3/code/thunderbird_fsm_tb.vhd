--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : ECE_template_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C9C John Doe, C3C Jacob Dunn
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : Include all documentation statements in main .vhd file
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : thunderbird_fsm.vhd
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;
  
entity thunderbird_fsm_tb is
end thunderbird_fsm_tb;

architecture test_bench of thunderbird_fsm_tb is 
	
  -- declare the component of your top-level design unit under test (UUT)
  component thunderbird_fsm is
    port(
	-- Identify input and output bits here
	   i_clk       : in std_logic;
	   i_reset     : in std_logic;
	   i_left      : in std_logic;
	   i_right     : in std_logic;
	   o_lights_L  : out std_logic_vector (2 downto 0);
	   o_lights_R  : out std_logic_vector (2 downto 0)
    );	
  end component;

  -- declare any additional components required
    signal w_left : std_logic := '0';
    signal w_right : std_logic := '0';
    signal w_reset : std_logic := '0';
    signal w_clk : std_logic := '0';
  -- declare signals needed to stimulate the UUT inputs
    signal w_lights_L : std_logic_vector (2 downto 0);
    signal w_lights_R : std_logic_vector (2 downto 0);
  -- also need signals for the outputs of the UUT
    constant k_clk_period : time := 10 ns;
  
begin
	-- PORT MAPS ----------------------------------------

	-- map ports for any component instances (port mapping is like wiring hardware)
	uut_inst : thunderbird_fsm port map (
	  -- use comma (not a semicolon)
	  -- no comma on last line
      i_left 	      => w_left,
      i_right 	      => w_right,
      i_reset         => w_reset,
      i_clk           => w_clk,
      o_lights_L(0)      => w_lights_L(0),
      o_lights_L(1)      => w_lights_L(1),
      o_lights_L(2)      => w_lights_L(2),
      o_lights_R(0)      => w_lights_R(0),
      o_lights_R(1)      => w_lights_R(1),
      o_lights_R(2)      => w_lights_R(2)
	);


	-- CONCURRENT STATEMENTS ----------------------------

	
	-- PROCESSES ----------------------------------------

	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- You will at least have a test process
	clk_proc : process
    begin
        w_clk <= '0';
        wait for k_clk_period/2;
        w_clk <= '1';
        wait for k_clk_period/2;
    end process;
	
	-- Test Plan Process --------------------------------
	-- Implement the test plan here.  Body of process is continuous from time = 0  
	test_process : process 
	begin
        w_reset <= '1';
        wait for k_clk_period*1;
    
        w_reset <= '0';
        wait for k_clk_period*1;		
		
        w_left <= '0', '1' after 10 ns, '0' after 30 ns, '1' after 80 ns, '0' after 150 ns, 
                       '1' after 180 ns, '0' after 210 ns, '1' after 230 ns, '0' after 250 ns, 
                       '1' after 260 ns, '0' after 290 ns, '1' after 340 ns, '0' after 370 ns,
                       '1' after 390 ns, '0' after 400 ns, '1' after 470 ns, '0' after 480 ns;
                       
        w_right <= '0', '1' after 50 ns, '0' after 70 ns, '1' after 110 ns, '0' after 140 ns,
                        '1' after 160 ns, '0' after 220 ns, '1' after 230 ns, '0' after 250 ns, 
                        '1' after 300 ns, '0' after 320 ns, '1' after 340 ns, '0' after 370 ns,
                        '1' after 430 ns, '0' after 440 ns, '1' after 470 ns, '0' after 480 ns;
        
        wait for k_clk_period*28;
        w_reset <= '1', '0' after 10 ns, '1' after 40 ns, '0' after 50 ns, '1' after 80 ns, '0' after 90 ns;
        
		wait;
	end process;	
	-----------------------------------------------------	
	
end test_bench;
