--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : ECE_template.vhd
--| AUTHOR(S)     : C9C John Doe, C3C Jacob Dunn
--| CREATED       : 01/01/2017
--| DESCRIPTION   : This file simply provides a template for all VHDL assignments
--| 				- Be sure to include your Documentation Statement below!
--|
--| DOCUMENTATION : None.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : None.
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : LIST ANY DEPENDENCIES
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--|    OFF: 00000001
--|    ON:  00000010
--|    R1:  00000100
--|    R2:  00001000
--|    R3:  00010000
--|    L1:  00100000
--|    L2:  01000000
--|    L3:  10000000
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

library unisim;
  use UNISIM.Vcomponents.ALL;

-- entity name should match filename  
entity thunderbird_fsm is 
  port(
	-- Identify input and output bits here
	i_clk, i_reset : in std_logic;
	i_left, i_right : in std_logic;
	o_lights_L : out std_logic_vector(2 downto 0);
	o_lights_R : out std_logic_vector(2 downto 0)
  );
end thunderbird_fsm;

architecture thunderbird_fsm_arch of thunderbird_fsm is 
	-- include components declarations and signals
	signal f_Q : std_logic_vector (7 downto 0) := "00000001";
    signal f_Q_next : std_logic_vector (7 downto 0) := "00000001";
	-- intermediate signals with initial value
	-- typically you would use names that relate to signal (e.g. c_mux_2)
  
begin
	-- PORT MAPS ----------------------------------------
    
	-- map ports for any component instantiations (port mapping is like wiring hardware)


	-- CONCURRENT STATEMENTS "MODULES" ------------------
    f_Q_next(0) <= (f_Q(0) AND not i_left and not i_right) OR f_Q(1) OR f_Q(2) OR f_Q(5);
    f_Q_next(1) <= f_Q(0) AND i_left AND i_right;
    f_Q_next(2) <= f_Q(3);
    f_Q_next(3) <= f_Q(4);
    f_Q_next(4) <= f_Q(0) AND i_right AND NOT i_left;
    f_Q_next(5) <= f_Q(6);
    f_Q_next(6) <= f_Q(7);
    f_Q_next(7) <= f_Q(0) AND i_left AND NOT i_right;
    
    o_lights_L(2) <= f_Q(1) OR f_Q(5) OR f_Q(6) OR f_Q(7);
    o_lights_L(1) <= f_Q(1) OR f_Q(5) OR f_Q(6);
    o_lights_L(0) <= f_Q(1) OR f_Q(5);
    o_lights_R(2) <= f_Q(1) OR f_Q(2) OR f_Q(3) OR f_Q(4);
    o_lights_R(1) <= f_Q(1) OR f_Q(2) OR f_Q(3);
    o_lights_R(0) <= f_Q(1) OR f_Q(2);
	-- Provide a comment that describes each "module" as appropriate
	-- think of "modules" in this sense as groups of related statements
		

		-- PROCESSES ----------------------------------------
	register_proc : process ( i_clk, i_reset )
    begin
       if i_reset = '1' then
           f_Q <= "00000001";
       elsif (rising_edge(i_clk)) then
           f_Q <= f_Q_next;
       end if;
    end process register_proc;
	-- Provide a comment that describes each process
	-- block them off like the modules above and separate with SPACE
	-- Note, the example below is a local oscillator address counter 
	--	not related to other code in this file
	
end thunderbird_fsm_arch;
