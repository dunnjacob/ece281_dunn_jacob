--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 3/9/2018  MOdified by Capt Dan Johnson (3/30/2020)
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 4 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - Minumum FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern (additional functionality)
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, sevenSegDecoder, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(

		clk     :   in std_logic; -- native 100MHz FPGA clock
		
		-- Switches (16 total)
		sw  	:   in std_logic_vector(15 downto 0);
		
		-- Buttons (5 total)
		btnC	:	in	std_logic;					  -- GO
		btnU	:	in	std_logic;					  -- master_reset
		btnL	:	in	std_logic;                    -- clk_reset
		btnR	:	in	std_logic;	                  -- fsm_reset
		--btnD	:	in	std_logic;			
		
		-- LEDs (16 total)
		led 	:   out std_logic_vector(15 downto 0);

		-- 7-segment display segments (active-low cathodes)
		seg		:	out std_logic_vector(6 downto 0);

		-- 7-segment display active-low enables (anodes)
		an      :	out std_logic_vector(3 downto 0)
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
	-- declare components and signals
	component MooreElevatorController is
        Port ( i_clk     : in  STD_LOGIC;
               i_reset   : in  STD_LOGIC := '0';
               i_stop    : in  STD_LOGIC := '0';
               i_up_down : in  STD_LOGIC := '0';
               o_floor   : out STD_LOGIC_VECTOR (3 downto 0)     
               );
    end component MooreElevatorController;
    
    component clock_divider is
        generic ( constant k_DIV : natural := 2    ); -- How many clk cycles until slow clock toggles
                                                   -- Effectively, you divide the clk double this 
                                                   -- number (e.g., k_DIV := 2 --> clock divider of 4)
        port(  
                i_clk    : in std_logic;
                i_reset  : in std_logic;           -- asynchronous
                o_clk    : out std_logic           -- divided (slow) clock
        );
    end component clock_divider;
    
    component sevenSegDecoder is
            port(
                i_D : in std_logic_vector(3 downto 0);
                o_S : out std_logic_vector(6 downto 0)
                );
        end component sevenSegDecoder;
    
    component sevenSegDecoder2 is
             port(
                i_D : in std_logic_vector(3 downto 0);
                o_S : out std_logic_vector(6 downto 0)
                );
        end component sevenSegDecoder2;
        
     component thunderbird_fsm is 
              port(
                -- Identify input and output bits here
                i_clk, i_reset : in std_logic;
                i_left, i_right : in std_logic;
                o_lights_L : out std_logic_vector(2 downto 0);
                o_lights_R : out std_logic_vector(2 downto 0)
                );
         end component thunderbird_fsm; 
      
      component TDM4 is 
        generic ( constant k_WIDTH : natural  := 4); -- bits in input and output
          port ( i_CLK         : in  STD_LOGIC;
                 i_RESET         : in  STD_LOGIC; -- asynchronous
                 i_D3         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
                 i_D2         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
                 i_D1         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
                 i_D0         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
                 o_DATA        : out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
                 o_SEL        : out STD_LOGIC_VECTOR (3 downto 0)    -- selected data line (one-cold)
          );
          end component TDM4;
        
	
    signal w_elevator_clk     : STD_LOGIC;
    signal w_bird_clk : STD_LOGIC; 
    signal w_TDM_clk : STD_LOGIC; 
    signal w_floor   : std_logic_vector (3 downto 0);
    signal w_clk_reset : std_logic := '1';
    signal w_floor_reset : std_logic := '1';
    signal w_TDM_reset: STD_LOGIC;
    signal w_left : std_logic;
    signal w_right : std_logic;
    signal w_leds : std_logic_vector (5 downto 0);
    signal w_translate   : std_logic_vector (3 downto 0);
    signal w_sel : std_logic_vector (3 downto 0);
    signal w_firstSeg: std_logic_vector(6 downto 0); 
    signal w_secondSeg: std_logic_vector(6 downto 0);
    signal w_stop : STD_LOGIC;
    signal w_updown : STD_LOGIC; 
    signal w_desired : STD_LOGIC_VECTOR(3 downto 0);
    signal w_begin : STD_LOGIC := '0';  
    signal w_pickup : std_logic_vector(3 downto 0);
    signal w_dropoff : std_logic_vector(3 downto 0);
     
    --signal w_fsm_clk : std_logic;
  
begin
	-- PORT MAPS ----------------------------------------
   MooreElevatorController_isnt: MooreElevatorController
   port map(
        i_stop => w_stop,
        i_reset => w_floor_reset,
        i_up_down => w_updown,
        i_clk => w_elevator_clk,
        o_floor => w_floor
        );
        
    clock_divider_isnt1: clock_divider
    generic map ( k_DIV => 25000000 )
    port map(
        i_reset => w_clk_reset,
        i_clk => clk,
        o_clk => w_elevator_clk     
        );
        
    clock_divider_isnt2: clock_divider
    generic map ( k_DIV => 5000000 )
    port map(
        i_reset => w_clk_reset,
        i_clk => clk,
        o_clk => w_bird_clk     
                );
   
    clock_divider_isnt3: clock_divider
    generic map ( k_DIV => 500 )
    port map(
         i_reset => w_TDM_reset,
         i_clk => clk,
         o_clk => w_TDM_clk     
                   );
	
	sevenSegDecoder_isnt: sevenSegDecoder
	port map(
	    i_D => w_translate,
        o_S => w_firstSeg
	    );
	
	sevenSegDecoder2_isnt: sevenSegDecoder2
    port map(
         i_D => w_translate,
         o_S => w_secondSeg
                );
	    
	 thunderbird_fsm_isnt: thunderbird_fsm
        port map(
                i_left => w_left,
                i_right => w_right, 
                i_reset => btnR,
                i_clk => w_bird_clk,
                
                o_lights_L(2) => w_leds(5),
                o_lights_L(1) => w_leds(4),
                o_lights_L(0) => w_leds(3),
        
                o_lights_R(2) => w_leds(2),
                o_lights_R(1) => w_leds(1),
                o_lights_R(0) => w_leds(0)
                );
      
      TDM_inst: TDM4
       port map(
            i_clk => w_TDM_clk,
            i_reset => w_TDM_reset, 
            i_D0 => w_floor,
            i_D1 => w_floor,
            i_D2 => w_floor,
            i_D3 => w_floor,
            o_DATA => w_translate,
            o_SEL => w_sel
            );
            

	-- CONCURRENT STATEMENTS ----------------------------
	
	-- ground unused LEDs (which is all of them for Minimum functionality)
	led(9 downto 6) <= (others => '0');
    
    
    w_left <= '1' when w_updown = '0' and w_stop = '0' and w_floor /= "1111" 
                else '0';
    

    w_right <= '1' when w_updown = '1' and w_stop = '0' and w_floor /= "0001" and w_floor /= "0000"
                else '0'; 
    
    w_clk_reset <= (btnL or btnU);
    w_floor_reset <= (btnR or btnU);
    
    led(10) <= w_leds(2); --and not w_floor(3);
    led(11) <= w_leds(2); --and not w_floor(3);
    led(12) <= w_leds(1); --and not w_floor(3);
    led(13) <= w_leds(1); --and not w_floor(3);
    led(14) <= w_leds(0); --and not w_floor(3);
    led(15) <= w_leds(0); --and not w_floor(3);
    
    led(5) <= w_leds(5); --and not w_floor(0);
    led(4) <= w_leds(5); --and not w_floor(0);
    led(3) <= w_leds(4); --and not w_floor(0);
    led(2) <= w_leds(4); --and not w_floor(0);
    led(1) <= w_leds(3); --and not w_floor(0);
    led(0) <= w_leds(3); --and not w_floor(0);
    
    w_desired <= w_dropoff when w_floor = w_pickup else w_pickup when btnC = '1';
    
    w_stop <= '1'when w_floor = w_desired or (w_floor = "0001" and w_desired = "0000") else '0'; 
    w_updown <= '1' when w_desired > w_floor else '0'; 
    
    w_pickup <= sw(15 downto 12);
    w_dropoff <= sw(3 downto 0);
    
	-- leave unused switches UNCONNECTED
	
	-- Ignore the warnings associated with these signals
	-- Alternatively, you can create a different board implementation, 
	--   or make additional adjustments to the constraints file
	
	-- wire up active-low 7SD anodes (an) as required
	-- Tie any unused anodes to power ('1') to keep them off    

	
	--an <= (0 => w_7SD_EN_n, others => '1');
	seg <= w_firstSeg when w_sel = "0111" or w_sel = "1011" else w_secondSeg; 
	
	an(0)   <= '1';
    an(1)   <= '1';
    an(2)   <= '0' when w_sel = "0111" or w_sel = "1011" else '1';
    an(3)   <= '0' when w_sel = "1101" or w_sel = "1110" else '1';
	
end top_basys3_arch;
